# GauntLite #

This will be a dungeon explore game similar to Gauntlet. Each level will be a labrynth style map, entirely visible from the top down. 

Levels will feature a combo of any or all of the following: enemies, positive or negative affecting items/map features, carryable/wearable items, 
interactive map features (buttons that open times doors, etc, a puzzle to solve, and a goal to reach (always a goal).

For conflict type, I'm thinking the story is kids in a nightmare. That way the violence could be stuffed animals and the enemies just disappear in a puff. 

Obviously, the story is to escape the nightmare. There would be something like 10-20 levels per area, all with the same area theme and level clear goal type. The last level goal is a portal (stairs, stargate, whatever) to the new area, styled like the new area will be.

### PCs ###

There are a few characters with different attack/defense in a RPS/RPSLS strength and weakness array. 

Source has: 

* archer elf (+range, + speed, -defense - strength, =magicA, = magicD)
* wizard (+magicA, +magicD, +range, =speed, -defense, -strength)
* armored (dude) warrior (+strength, +defense, =speed, -range, -magicA, =magicD)
* agile (lady) warrior (+speed, = strength, =defense, -range, =magicA, =magicD). 

For the nightmare and kids conflict, can do combinations of speed, attack power, defense power, range, magic attack, magic defense to balance characters (possibly have players allocate their own points to each attribute of a finite amount?)

Strength and weaknesses would be based on their outfit and weapon choice, which would make it so there's no strength difference between boys and girls. Could even put the outfit together programmatically based on the user-allocated attributes (slow speed = winter boots, fast speed = sneakers, high defense = strapped pillows, low defense = shorts + shirt). Would need to generate and store all of the possible sprite combos, which might not be too bad.

* Pillows belted around torso, snow boots, winter hat, swung pillow weapon = warrior
* Hooded footy pajamas, rubber ducky thrown weapon = archer elf 
* Bunny slippers, jeans, t-shirt, hat, thrown water balloons = wizard
* Sneakers, shorts, tank top, swung stuffed rabbit = agile warrior

### NPCs ###

Enemies will be similarly balanced with obvious signs of their strength/weaknesses, perhaps a glossary. Since it's a nightmare, these can be area-relevant things like frogs, spiders, mice, tv remote controls, broccoli, candy, school supplies, etc.

### Puzzles ###

Puzzles will feature any number of sequential door unlocks (enemies or chests carrying keys), timing traps (intermittent flames, swinging blades, etc), blocks that need to be moved onto sensor plates, secret hidden walls (like Zelda). These will be made more difficult with enemies, pits, etc.

### Goal ###

The goal for each level will be the same in each area. A doorway probably since we wouldn't expect to portal out and stay in the area. Once the player touches the goal, that is a win. Each level will feature sub goals clear time and item use, enemy kills for 3-star rating. 

### Bosses? ###

Maybe.

### Multiplayer Coop? ###

Maybe. Would need to add enemy challenge if so.

### Ending ###

After the final goal, begin horde mode (endless survival)?